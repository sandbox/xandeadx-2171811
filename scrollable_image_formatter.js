(function ($) {
  Drupal.behaviors.scrollableImageFormatter = {
    attach: function (context, settings) {
      $('.scrollable-image-slides-wrapper').once('scrollable-image-slides-wrapper').scrollable({
        items: '.scrollable-image-slides',
        prev: '.scrollable-image-control-prev',
        next: '.scrollable-image-control-next'
      });

      $('.scrollable-image-slide a').once('scrollable-image-item').click(function (event) {
        var $originalImageLink = $(this).closest('.scrollable-image').find('.scrollable-image-original a');
        var $originalImage = $originalImageLink.find('img');

        $originalImageLink.attr('href', this.href);
        $originalImage.attr('src', $(this).data('style-url'));

        event.preventDefault();
      });
    }
  };
})(jQuery);
